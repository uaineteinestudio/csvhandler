# CSVReader

A lightweight CSV parser that takes in a comma delimited file and parses it as a 2D object array.

## Getting Started

Compling yourself: Use Visual Studio and the project file to create either a new project to be modified or add to a solution for use as a DLL.

### Requires:

This project is target for .NET standard 2. It should be compliant with earlier versions of course, however when compiling with VS, one will have to change the project settings.

### Installing

##### Building with Windows:

Use Visual Studio with the project as a reference to build the project file into an DLL for an app.

##### Building with Linux:

Open terminal/cmd and use .NET core to build with:

```
dotnet publish -c release -r ubuntu.16.04-x64 --self-contained
```

### Calling

Call by single method using CSV for either a single column or multi column CSV file. The CSV reader takes them as objects so casting is of your choice.
```
//single column
singlecolf = "Namelists/MaleFirstNames.csv";
object[] singlecol = CSV.ReadAllSC(singlecolf);

//multi
multicolf = "Namelists/Titles.csv";
object[,] = CSV.ReadAllMC(multicolf);
```

## Built With
* [CSVHelper](https://joshclose.github.io/CsvHelper/) - Josh Close  [github](https://github.com/JoshClose/CsvHelper)


## Authors

* **Daniel Stamer-Squair** - *UaineTeine*

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details