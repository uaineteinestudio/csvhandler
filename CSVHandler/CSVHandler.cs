﻿using System.Collections.Generic;
using System.IO;
using CsvHelper;
using System.Globalization;

namespace csvf
{
    public static class CSV
    {
        public static object[] ReadAllSC(string filename)
        {
            List<object> data = new List<object>();
            using (StreamReader reader = new StreamReader(filename))
            {
                CsvReader csvreader = new CsvReader(reader, CultureInfo.InvariantCulture);
                int index = 0;
                while (csvreader.Read())
                {
                    data.Add(csvreader[0]);
                    index += 1;
                }
                reader.Close();
                csvreader.Dispose();
            }
            return data.ToArray();
        }
        public static object[,] ReadAllMC(string filename)
        {
            List<List<object>> data = new List<List<object>>();
            using (StreamReader reader = new StreamReader(filename))
            {
                CsvReader csvreader = new CsvReader(reader, CultureInfo.InvariantCulture);
                int index = 0;
                while (csvreader.Read())
                {
                    data.Add(new List<object>());
                    int i = 0;
                    while (true)
                    {
                        try
                        {
                            data[index].Add(csvreader[i]);
                            i += 1;
                        }
                        catch
                        {
                            break;
                        }
                    }
                    index += 1;
                }
                reader.Close();
                csvreader.Dispose();
            }
            object[,] output = new object[data.Count, data[0].Count];
            for (int i = 0; i < data.Count; i++)
                for (int k = 0; k < data[0].Count; k++)
                    output[i, k] = data[i][k];
            return output;
        }
    }
}
